import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ProjectService} from "./services/project/project.service";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {ProjectsFilter} from "./filters/projects.filter";
import {RouterModule, Routes} from "@angular/router";
import { DetailComponent } from './detail/detail.component';
import { OverviewComponent } from './overview/overview.component';
import { PaymentComponent } from './payment/payment.component';

export const ROUTES: Routes = [
    { path: '', redirectTo: 'projects', pathMatch: 'full' },
    { path: 'projects', component: OverviewComponent },
    { path: 'projects/:id', component: DetailComponent },
    { path: 'payment', component: PaymentComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ProjectsFilter,
    DetailComponent,
    OverviewComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [ProjectService],
  bootstrap: [AppComponent]
})

export class AppModule {}
