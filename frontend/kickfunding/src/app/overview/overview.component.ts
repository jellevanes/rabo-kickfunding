import { Component, OnInit } from '@angular/core';
import {ProjectService} from "../services/project/project.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  ngOnInit() {
  }

    private projects = [];
    private searchText = "";

    constructor(private projectService: ProjectService, private router: Router) {
        projectService.getProjects().subscribe(data => {
            this.projects = data.json();
        }, error => {

        });
    }

    openDetail(id) {
        this.router.navigate(['/projects', id]);
    }

}
