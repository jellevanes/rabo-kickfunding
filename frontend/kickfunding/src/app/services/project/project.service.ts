import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ProjectService {

  constructor(private http: Http) { }

  public getProjects(): Observable<Response> {
      return this.http.get('assets/data/projects/projects.json');
  }

    public getProjectById(id): Observable<Response> {
        return this.http.get('assets/data/details/' + id + '.json');
    }

}
