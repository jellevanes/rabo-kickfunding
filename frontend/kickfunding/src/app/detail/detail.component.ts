import { Component } from '@angular/core';
import {ProjectService} from "../services/project/project.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent {

  private project = {};

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private router: Router) {
      projectService.getProjectById(this.route.snapshot.params['id']).subscribe(data => {
          this.project = data.json();
      }, error => {

      });
  }

  openPayment() {
      this.router.navigate(['/payment']);
  }

  goBack() {
      this.router.navigate(['/projects']);
  }
}
